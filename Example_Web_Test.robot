*** Settings ***
Documentation   = Example Web Test =
...
...             == Summary ==
...
...             A small demo of _Robot Framework_ testing against a simple website.
...
...             == Notes ==
...
...             - No demonstration of parameters used.
...             - Many more libraries can be found [http://robotframework.org/#test-libraries|here].
...
...             == Description ==
...
...             Using a re-purposed simple Flask demo
...             [http://flask.pocoo.org/docs/0.10/tutorial/|Flaskr],
...             our example test suite simply logs in, creates a blog entry,
...             then checks for the result, and quits.
...
...             === Steps ===
...
...             | *Step* | *Action* |
...             | _1._ | Open browser to the locally-hosted Flaskr page. |
...             | _2._ | Logs in using the standard credentials. |
...             | _3._ | Creates a sample blog entry using 'faked' text strings. |
...             | _4._ | Verifies the blog entry name and text are present. |
...             | _5._ | Quits the browser. |
...
...             ---

Suite Setup         Prep Flaskr Test
Suite Teardown      Close Browser
Resource            rf_resources.robot

*** Variables ***
# Everything comes from rf_resources.txt and (indirectly) yaml_loader.py

*** Test Cases ***
Log In to Blog
    [Documentation]     = Case Steps =
    ...
    ...                 | *Step* | *Action* |
    ...                 | _1._ | Browser is launched in setup; go to Python web browser. |
    ...                 | _2._ | We verify that we're not already logged in. |
    ...                 | _3._ | Enter username and password; attempt login. |
    ...                 | _4._ | Verify that we're logged in successfully. |
    [Tags]  positive    login
    Log-in Page Looks Good
    Attempt Login
    Verify Logged In

Create & Verify Blog Post
    [Documentation]     = Case Steps =
    ...
    ...                 | *Step* | *Action* |
    ...                 | _1._ | Enter blog Title and Body; 'share' post. |
    ...                 | _2._ | Verify that blog Title and Body are now listed. |
    ...                 | _3._ | Browser is closed via teardown. |
    [Tags]  positive    functional
    Create Blog Post
    Verify Blog Entry

*** Keywords ***
# All Keywords are now located in the rf_resources.robot file

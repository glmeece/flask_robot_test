*** Settings ***
Documentation     A resource file with reusable keywords and variables.
...
...               The system specific keywords created here form our own
...               domain specific language.

Library                     Selenium2Library
Library                     OperatingSystem
Library                     FakerLibrary    en_US

*** Variables ***
${SERVER_BASE}              http://127.0.0.1:5000/
${DELAY}                    0.35
${SLEEP_DEFAULT}            1
${TIMEOUT}                  10
${BROWSER}                  Chrome  # Selections: Chrome or Firefox
${USERNAME_FIELD}           id=username
${PASSWORD_FIELD}           id=password
${DEF_USERNAME}             admin
${DEF_PASSWORD}             default
${TITLE_FIELD}              name=title
${TEXT_FIELD}               name=text
${DBName}                   flaskr.db

*** Keywords ***

##### BROWSER SETUP SECTION #####
Open Browser to Page
    [Documentation]     Opens either Google Chrome, or Netscape Firefox
    ...                 to a given web page.
    [Arguments]    ${URL}
    Run Keyword If      '${BROWSER}' == 'Chrome'    Open Chrome Browser to Page       ${URL}
    ...     ELSE IF     '${BROWSER}' == 'Firefox'   Open Firefox Browser to Page      ${URL}
    Set Selenium Speed              ${DELAY}

Open Chrome Browser to Page
    [Documentation]     Opens Google Chrome to a given web page.
    [Arguments]    ${URL}
    ${chrome_options}=    Evaluate    sys.modules['selenium.webdriver'].ChromeOptions()    sys
    Call Method    ${chrome_options}    add_argument    test-type
    Call Method    ${chrome_options}    add_argument    disable-extensions
    Run Keyword If    os.sep == '/'    Create Webdriver    Chrome    my_alias    chrome_options=${chrome_options}    executable_path=/usr/lib/chromedriver
    ...    ELSE    Create Webdriver    Chrome    my_alias    chrome_options=${chrome_options}
    Set Window Size    750    900
    Go To    ${URL}

Open Firefox Browser to Page
    [Documentation]     Opens Netscape Firefox to a given web page.
    [Arguments]    ${URL}
    Open Browser  ${URL}  firefox
    # For some reason FF wants to start with the window way over to the right.
    Set Window Position   5   5
    Set Window Size    750    900

Open App Page
    [Documentation]     Opens browser to base web page.
    Open Browser to Page    ${SERVER_BASE}

Prep Flaskr Test
    Open App Page

##### UTILITY SECTION #####

Return OS
    [Documentation]     Determines whether we're executing on a Windows box or
    ...                 Linux box.
    ${is_equal} =           Run Keyword And Return Status   Should Be Equal As Strings    /    ${/}
    ${os_name} =            Set Variable If    ${is_equal}     Linux   Windows
    [Return]                ${os_name}

Clear Flaskr Database
    [Documentation]
    Connect To Database Using Custom Params    sqlite3    database="./${DBName}"
    Sleep                   2 seconds
    Delete All Rows From Table      entries
    @{queryResults}         Query     select * from entries;
    Log to Console          Entries @{queryResults} records.

Scroll to Element
    [Documentation]     Pass in a recognizable element selector; this keyword
    ...                 will scroll the pane to make it the top-most element.
    [Arguments]             ${the_element}
    Wait Until Element Is Visible   ${the_element}     timeout=5 seconds    error=φ Scroll to Element:: unable to locate the element '${the_element}'
    ${vert_pos} =           Get Vertical Position    ${the_element}
    ${horz_pos} =           Get Horizontal Position    ${the_element}
    Execute JavaScript      window.scrollTo(${horz_pos},${vert_pos})
    Sleep                   .25 seconds

Boxed String
    [Documentation]     Pass in a string - get it back in a "shadowed" box!
    ...                 If you set start_of_list to False, then you won't get
    ...                 the dangling list line.
    ...                 Note: You may need to prepend a newline to the
    ...                 returned string to avoid wrapping issues.
    [Arguments]             ${string_in}  ${start_of_list}=True
    ${last_line_under} =    Set Variable If    '${start_of_list}' == 'True'    ╤   ═
    ${str_len} =            Get Length      ${string_in}
    ${horiz_sing} =         Set Variable    ─
    ${horiz_doub} =         Set Variable    ═
    ${first_line} =         Catenate    SEPARATOR=  ┌   ─   ${horiz_sing * ${str_len}}  ─   ╖
    ${middle_line} =        Catenate                │       ${string_in}                    ║
    ${last_line} =          Catenate    SEPARATOR=  ╘   ${last_line_under}   ${horiz_doub * ${str_len}}  ${horiz_doub}   ╝
    ${all_lines} =          Catenate    SEPARATOR=  ${first_line}   \n  ${middle_line}  \n  ${last_line}
    [Return]                ${all_lines}

##### FUNCTIONAL TESTING SECTION #####

Log-in Page Looks Good
    [Documentation]     Verifies that we've landed on the start page.
    ${boxed_landing} =          Boxed String    Verifying Landing Page  start_of_list=False
    Log to Console              \n${boxed_landing}
    Set Test Message            *HTML* <b>Test Case Summary</b>
    Wait Until Page Contains    log in      timeout=${TIMEOUT}
    ${success_msg} =            Set Variable   Successfully arrived at home page!
    Set Test Message            <li>${success_msg}</li>    append=yes
    Log to Console              \n${SPACE}√ ${success_msg}\n

Attempt Login
    [Documentation]     Verifies that we've logged in.
    ${boxed_landing} =          Boxed String    Attempting Log-in
    Log to Console              \n${boxed_landing}
    Click Link                  link=log in
    Verify Login Page
    Log to Console              ${SPACE}├ Username: '${DEF_USERNAME}'
    Focus                       ${USERNAME_FIELD}
    Input Text                  ${USERNAME_FIELD}    ${DEF_USERNAME}
    Log to Console              ${SPACE}├ Password: '${DEF_PASSWORD}'
    Focus                       ${PASSWORD_FIELD}
    Input Text                  ${PASSWORD_FIELD}    ${DEF_PASSWORD}
    Log to Console              ${SPACE}└ Clicking 'Login' button
    Click Button                Login

Verify Login Page
    Wait Until Page Contains    Username  timeout=${TIMEOUT}  error=Couldn't find the login form!

Verify Logged In
    Wait Until Page Contains    You were logged in  timeout=${TIMEOUT}  error=Couldn't verify we logged in!
    ${success_msg} =            Set Variable   Logged in!
    Set Test Message            <li>${success_msg}</li>    append=yes
    Log to Console              \n${SPACE}√ ${success_msg}\n
    Sleep                       1 seconds

Create Blog Post
    [Documentation]     Enters Title and Text for blog post.
    ${boxed_landing} =          Boxed String    Attempting Blog Post Creation
    Log to Console              \n${boxed_landing}
    ${fake_title} =             FakerLibrary.Catch Phrase
    ${fake_text} =              FakerLibrary.Paragraph  nb_sentences=1
    Log to Console              ${SPACE}├ Title: '${fake_title}'
    Focus                       ${TITLE_FIELD}
    Input Text                  ${TITLE_FIELD}    ${fake_title}
    Log to Console              ${SPACE}├ Text: '${fake_text}'
    Focus                       ${TEXT_FIELD}
    Input Text                  ${TEXT_FIELD}    ${fake_text}
    Log to Console              ${SPACE}└ Clicking 'Share' button
    Click Button                Share
    Set Suite Variable          ${BLOG_TITLE}   ${fake_title}
    Set Suite Variable          ${BLOG_TEXT}    ${fake_text}
    Sleep                       1 seconds

Verify Blog Entry
    [Documentation]     Verifies entry of blog post.
    ${boxed_landing} =          Boxed String    Verifying Blog Entry
    Log to Console              \n${boxed_landing}
    Set Test Message            *HTML* <b>Test Case Summary</b>
    Wait Until Page Contains    ${BLOG_TITLE}  timeout=${TIMEOUT}  error=Couldn't find the blog title ${BLOG_TITLE}!
    Log to Console              ${SPACE}├ Found: '${BLOG_TITLE}'!
    Wait Until Page Contains    ${BLOG_TEXT}  timeout=${TIMEOUT}  error=Couldn't find the blog title ${BLOG_TEXT}!
    Log to Console              ${SPACE}└ Found: '${BLOG_TEXT}'!
    ${success_msg} =            Set Variable   Created a blog entry!
    Set Test Message            <li>${success_msg}</li>    append=yes
    Log to Console              \n${SPACE}√ ${success_msg}\n
    Sleep                       3 seconds





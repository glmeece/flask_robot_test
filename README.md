# flask_robot_test

## Metadata

* Title: **Flask Robot Test Demo Read Me**
* Author: **Greg Meece** @glmeece
* Affiliation: **Vigilant Watchman, LLC.**
* Date: **March 23, 2016**

## Repo Location

https://bitbucket.org/glmeece/flask_robot_test

## Purpose

This is just a simple web app designed to demonstrate [Robot Framework](http://robotframework.org/)'s capacity to test against a website using the [Selenium 2 (Webdriver) library](http://robotframework.org/Selenium2Library/doc/Selenium2Library.html).

This was prepared in particular for [Global Pueblo](http://globalpueblo.com/)'s Chandigarh team. :earth_asia:

It is based on the [Flaskr micro-blog example app](https://github.com/mitsuhiko/flask/tree/master/examples/flaskr) from the [Flask website](http://flask.pocoo.org/) and [repository](https://github.com/mitsuhiko/flask).

## General Setup Info

* You should have [Python 2.7.x](https://www.python.org/downloads/release/python-2711/) installed. There is no telling what will break if you go with Python 3.x.
* You should also have `pip` installed. Most Python distributions come with it now, but be sure by trying: `pip --version`. If you get an error message, [you'll need to download/install for your platform](https://pip.pypa.io/en/latest/installing/).
* All of the following commands should be executed in a terminal. If you're on Linux or Mac OS X and want them installed universally, pre-pend these commands with `sudo`.

## Installing Flask/Setting Up Flaskr

Assuming you already have Python installed, along with `pip`, then:
```bash
pip install https://github.com/mitsuhiko/flask/tarball/master
pip install pytest
```

## Flaskr Setup

1. You may need to edit the configuration in the `flaskr.py` file (starts at line 23) or export an FLASKR_SETTINGS environment variable pointing to a configuration file.
2. Initialize the database with this terminal command: `flask --app=flaskr initdb`
3. Now you can run flaskr: `flask --app=flaskr run`
4. You can find the app running here: [http://localhost:5000/](http://localhost:5000/)

### Testing the Flaskr App Itself

Run the `test_flaskr.py` file to see the tests pass.

## Robot Framework Tests

You'll need to install Robot Framework and the libraries this test uses before execution.

### Install Robot Framework and Libraries

Execute the following:
```bash
pip install robotframework
pip install robotframework-selenium2library
pip install robotframework-faker
```

### Running Tests

Execute:
```bash
robot Example_Web_Test.robot
```
If you want to see how many test cases are in your total collection, you would include the path to the root of the test cases, and use the `--dryrun` parameter in the call. In our above example, we would invoke:
```bash
robot --dryrun Example_Web_Test.robot
```
You would see that it "simulates" running all the test cases, and gives you a quick count of the cases at the end (2 in our example).

### Generating Test Documenation

To produce an HTML page document which formats the in-place testing documentation, execute:
```bash
python -m robot.testdoc -T "Automated Testing Documentation" -N "All Test Suites" -D "Overview of Robot Framework Automated Test Suites" Example_Web_Test.robot rf_tests_documentation.html
```